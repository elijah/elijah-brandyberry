// Predefined vars
var doc = $(document),
    win = $(window),
    screen = {
      height: win.height(),
      width: win.width(),
      ratio: screen.width/screen.height
    },
    sectionsScroll = [],
    currentSection = 0,
    // Background image dimensions in here
    bgImg = {
      height: 1080,
      width: 1920
    };
bgImg.ratio = bgImg.width/bgImg.height;



var js = {
  init: function() {
    js.animations.fadeInDelay();
    js.animations.fadeInScrollStart();
    js.animations.backgroundScroll();
    js.animations.fadeInScrollDelay();
    js.handlers.anchorLinkClick();
    js.handlers.showHeaderOnScroll();
    js.handlers.moveToSection();
    js.handlers.windowResize();
    js.handlers.docScroll();
    js.setup.showNav();
    js.setup.updateScreenSize();
    js.setup.fitBackground();
    js.setup.getCurrentSection();
    js.setup.getSectionScrolls();
  },
  setup: {
    getSectionScrolls: function() {
      var sections = $('.section');
      sectionsScroll = [];
      sections.each(function() {
        var section = $(this);
        sectionsScroll.push(section.offset().top)
      });
    },
    showNav: function() {
      if (doc.scrollTop() > 0) {
        doc.trigger('scroll')
      }
    },
    fitBackground: function() { 
      var body = $('body'),
          minHeight = $('#section-0').outerHeight(true),
          setHeight = screen.height;
      if (setHeight < minHeight) {
        setHeight = minHeight
      }
      if (screen.ratio < bgImg.ratio) {
        body.css({
          backgroundSize: 'auto '+setHeight+'px'
        });
      } else {
        body.css({
          backgroundSize: '100vw auto'
        });
      }
    },
    updateScreenSize: function() {
      screen.height = win.height();
      screen.width = win.width();
      screen.ratio = screen.width/screen.height;
    },
    getCurrentSection: function() {
      var scroll = doc.scrollTop() + 51,
          s = 0;
      while (sectionsScroll[s+1] < scroll) {
        s++;
      }
      currentSection = s;
    }
  },
  handlers: {
    anchorLinkClick: function() {
      doc.on('click', 'a', function(e){
        var anchor = $.attr(this, 'href');
        if (anchor[0] == '#') {
          e.preventDefault();
          js.animations.animateAnchorLink(anchor, $(this).data('section'))
        }
      });
    },
    showHeaderOnScroll: function() {
      var unfixedPos = 1;
      $('.fixed-brand-header').css('top', '0px')
      doc.on('touchmove', function(e) { 
        unfixedPos = js.animations.showHeader(unfixedPos);
      });
      doc.on('scroll', function(e) {
        unfixedPos = js.animations.showHeader(unfixedPos);
      });
    },
    windowResize: function() {
      win.resize(function() {
        js.setup.updateScreenSize();
        js.setup.fitBackground();
        js.setup.getSectionScrolls();
        js.setup.getCurrentSection();
      });
    },
    docScroll: function() {
      doc.scroll(function() {
        js.setup.getCurrentSection();
      });
    },
    moveToSection: function() {
      $('.arrow-btns button').on('click', function() {
        var action = $(this).data('action'),
            section = currentSection,
            anchor,
            scroll = doc.scrollTop();
        if (sectionsScroll[section] > scroll || action > 0) {
          section += action;
          if (section < 0 || section >= sectionsScroll.length) {
            section -= action;
            return;
          }
        }
        anchor = '#section-'+section
        js.animations.animateAnchorLink(anchor, section)
      });
    }
  },
  helpers: {
    fadeIn: function(elem) {
      elem.animate({
        opacity: 1
      }, 500);
    }
  },
  animations: {
    fadeInDelay: function() {
      var elem = $('.load-fade-in');
      elem.css({
        opacity: 0
      });
      setTimeout(function() {
        js.helpers.fadeIn(elem)
      }, 400);
    },
    fadeInScrollStart: function() {
      var elem = $('.scroll-fade-in');
      elem.css({
        opacity: 0
      });
      doc.on('scroll', function(e) {
        js.helpers.fadeIn(elem)
      });
    },
    fadeInScrollDelay: function() {
      var elem = $('.delay-scroll-fade-in');
      elem.css({
        opacity: 0
      });
      doc.on('scroll', function(e) {
        setTimeout(function() {
          js.helpers.fadeIn(elem)
        }, 400);
      });
    },
    showHeader: function(unfixedPos) {
      var scroll = doc.scrollTop(),
          header = $('#brand-header'),
          headerPos = header.position().top;
      $('.fixed-brand-header').css('top', scroll+'px')
      if (header.hasClass('fixed-brand-header')) {
        if (scroll <= unfixedPos) {
          header.removeClass('fixed-brand-header')
          header.css('top', unfixedPos+'px')
          setTimeout(function() {
            header.css('top', '')
          }, 500)
        }
      } else {
        if (scroll > headerPos) {
          unfixedPos = headerPos
          header.addClass('fixed-brand-header')
        }
      }
      return unfixedPos
    },
    backgroundScroll: function() {
      doc.on('scroll', function(e) {
        var scroll = doc.scrollTop(),
            body = $('body');
        body.css({
          backgroundPosition: '50% '+ (scroll/3) +'px'
        });
      });
    },
    animateAnchorLink: function(anchor, section) {
      $('html, body').animate({
          scrollTop: $(anchor).offset().top - 50
      }, 500);
      currentSection = section;
    }
  }
}

$(function() {
  js.init();
});